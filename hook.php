if<?php
        $CHROMECAST_SALON_UUID = "uuidtocheck";
        $plex_webhooks_json = json_decode($_POST["payload"],true);
        if ($plex_webhooks_json['Player']['uuid'] == $CHROMECAST_SALON_UUID) {
                if ($plex_webhooks_json['event'] == 'media.play') {
                    $Value_plex_action = 'Salon_Play';
                }elseif ($plex_webhooks_json['event'] == 'media.pause') {
                    $Value_plex_action = 'Salon_Pause';
                }elseif ($plex_webhooks_json['event'] == 'media.resume') {
                    $Value_plex_action = 'Salon_Resume';
                }elseif ($plex_webhooks_json['event'] == 'media.stop') {
                    $Value_plex_action = 'Salon_Stop';
                }
        }
        else
        {
                syslog(LOG_WARNING, "Client : " . $plex_webhooks_json['Player']['uuid']);
        }
        if(isset($Value_plex_action))
        {
                copy('https://maker.ifttt.com/trigger/' . $Value_plex_action . '/with/key/keyhere', 'result.txt');
        }
?>
